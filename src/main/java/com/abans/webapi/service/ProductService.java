package com.abans.webapi.service;

import com.abans.webapi.dto.CategoryProductDto;
import com.abans.webapi.dto.ProductDto;
import com.abans.webapi.model.Product;

import java.util.List;

public interface ProductService {
    public List<ProductDto> readAllBySubCategoryName(String name) throws Exception;
    public ProductDto findById(String id) throws Exception;
    public List<CategoryProductDto> readAllByCategory() throws Exception;
}
